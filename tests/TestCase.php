<?php
namespace Eblix\Mailchimp\Test;

use Eblix\Mailchimp\EblixMailchimpFacade;
use Eblix\Mailchimp\EblixMailchimpServiceProvider;

class TestCase extends \Illuminate\Foundation\Testing\TestCase
{
    /**
     * Load package service provider
     * @param  \Illuminate\Foundation\Application $app
     * @return  \Eblix\Mailchimp\EblixMailchimpServiceProvider
     */
    protected function getPackageProviders($app)
    {
        return [EblixMailchimpServiceProvider::class];
    }

    /**
     * Load package alias
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageAliases($app)
    {
        return [
            'EblixMailchimp' => EblixMailchimpFacade::class,
        ];
    }
}