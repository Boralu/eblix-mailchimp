<?php

use Eblix\Mailchimp\Models\MailchimpList;
use Eblix\Mailchimp\Models\Member;

class EblixMailchimpTest extends \Tests\TestCase
{
    public function testListCreate()
    {
        Session::start();

        $credentials = array(
            'list_name' => 'testlist',
            'from_email' => 'test2@test.com',
            'from_name' => 'test form',
            'subject' => 'test subject',
            'language' => 'en',
            'email_type_option' => true,
            'permission_reminder' => 'You are receiving this email because you opted in via our website.',
            'company' => 'test company',
            'address1' => 'address1',
            'address2' => 'address2',
            'city' => 'colombo',
            'state' => 'colombo',
            'zip' => '0700',
            'country' => 'LK',
            'phone' => '1234567890',
            '_token' => csrf_token()
        );

        $response = $this->call('POST', '/mailchimp/create', $credentials);
        $response->assertSessionHas('message', 'List Created Successfully!');
    }

    public function testListUpdate()
    {
        Session::start();
        $List = MailchimpList::orderBy('id','DESC')->first();

        $credentials = array(
            'list_name' => 'testlist updated',
            'from_email' => 'test2@test.com',
            'from_name' => 'test form',
            'subject' => 'test subject',
            'language' => 'en',
            'email_type_option' => true,
            'permission_reminder' => 'You are receiving this email because you opted in via our website.',
            'company' => 'test company',
            'address1' => 'address1',
            'address2' => 'address2',
            'city' => 'colombo',
            'state' => 'colombo',
            'zip' => '0700',
            'country' => 'LK',
            'phone' => '1234567890',
            '_token' => csrf_token()
        );

        $response = $this->call('POST', '/mailchimp/'.$List->id.'/update', $credentials);
        $response->assertSessionHas('message', 'List Updated Successfully!');
    }

    public function testListDelete()
    {
        Session::start();
        $List = MailchimpList::orderBy('id','DESC')->first();

        $response = $this->call('get', '/mailchimp/'.$List->id.'/delete', ['_token' => csrf_token()]);
        //$this->assertEquals(204, $response->getStatusCode());
        $response->assertSessionHas('message', 'List Deleted Successfully!');
    }

    public function testMemberCreate()
    {
        Session::start();
        $List = MailchimpList::orderBy('id','DESC')->first();

        $credentials = array(
            'email_address' => 'testmember@testmember.com',
            'status' => 'subscribed',
            'first_name' => 'test firstname',
            'last_name' => 'test lastname',
            '_token' => csrf_token()
        );

        $response = $this->call('POST', '/mailchimp/'.$List->id.'/members/create', $credentials);
        $response->assertSessionHas('message', 'Member Created Successfully!');
    }

    public function testMemberUpdate()
    {
        Session::start();
        $List = MailchimpList::orderBy('id','DESC')->first();
        $Member = Member::where('list_id', $List->id)->orderBy('id','DESC')->first();

        $credentials = array(
            'email_address' => 'testmember2@testmember.com',
            'status' => 'subscribed',
            'first_name' => 'test firstname',
            'last_name' => 'test lastname',
            '_token' => csrf_token()
        );

        $response = $this->call('POST', '/mailchimp/'.$List->id.'/members/'.$Member->id.'/update', $credentials);
        $response->assertSessionHas('message', 'Member Updated Successfully!');
    }

    public function testMemberDelete()
    {
        Session::start();
        $List = MailchimpList::orderBy('id','DESC')->first();
        $Member = Member::where('list_id', $List->id)->orderBy('id','DESC')->first();

        $response = $this->call('get', '/mailchimp/'.$List->id.'/members/'.$Member->id.'/delete', ['_token' => csrf_token()]);
        $response->assertSessionHas('message', 'Member Deleted Successfully!');
    }
}