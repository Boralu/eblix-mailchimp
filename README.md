# README

Laravel 5.5 package to create, update and delete MailChimp lists and members.

## Requirements

PHP version >= 7.0.0

CURL version ^7.1.7

Laravel Framework >= 5.5

## Instalation

First run composer command

<code>composer require eblix/eblix-mailchimp</code>

Then run migrations using 

<code>php artisan migrate</code>

Then run database seeds using 

<code>php artisan db:seed --class="Eblix\Mailchimp\Database\Seeds\CountryTableSeeder"</code>

Next step is to publish package

<code>php artisan vendor:publish --provider="Eblix\Mailchimp\EblixMailchimpServiceProvider"</code>


## Usage

### Config file

Package config can be found in mailchimp.php file under config directory (after you published it).
Config file contains **MailChimp API Key** which is required to connect with MailChimp and default values for the contact information for the list. 
You can edit default values here and passing parameter values to create and update lists.

    [
        'MAILCHIMP_API_KEY' => 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-usXX', //Your Mailchimp API Key which is required
    
        //Your Contact & Other Details
        
        'CONTACT_COMPANY' => 'Company Name',
        'CONTACT_ADDRESS1' => 'Address',
        'CONTACT_ADDRESS2' => 'Address',
        'CONTACT_CITY' => 'City',
        'CONTACT_STATE' => 'State',
        'CONTACT_ZIP' => 'Zip or postal',
        'CONTACT_COUNTRY' => 'US',
        'PERMISSION_REMINDER' => 'You are receiving this email because you opted in via our website.',
        'EMAIL_TYPE_OPTION' => true,
        'CAMPAIGN_FROM_NAME' => "Form Name",
        'CAMPAIGN_FROM_EMAIL' => "emailaddress@email.com",
        'CAMPAIGN_SUBJECT' => "",
        'CAMPAIGN_LANGUAGE' => "en",
    ]

### Customize

If you want to customize the apperance of the views of the package, you can find <code>views</code> and <code>assets</code> files in <code>resources/views/eblix/mailchimp</code> and <code>public/eblix/mailchimp</code> folders, respectively.

### Routes

Adding <code>/mailchimp</code> at the end of your base url, will navigate you to the MailChimp package.
