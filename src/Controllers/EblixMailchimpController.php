<?php

namespace Eblix\Mailchimp\Controllers;

use Eblix\Mailchimp\Models\Country;
use Eblix\Mailchimp\Models\MailchimpList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Eblix\Mailchimp\EblixMailchimp;

class EblixMailchimpController
{
    public function index(){
        return view('eblix.mailchimp.lists.list', ['lists' => MailchimpList::all()]);
    }

    public function create()
    {
        return view('eblix.mailchimp.lists.create', ['countries' => Country::all()]);
    }

    public function store(Request $request){
        $rules = array(
            'list_name' => 'required',
            'from_email' => 'required|email',
            'from_name' => 'required',
            'subject' => 'required',
            'company' => 'required',
            'address1' => 'required',
            'address2' => 'required',
            'permission_reminder' => 'required',
            'city' => 'required',
            'state' => 'required',
            'language' => 'required|not_in:0',
            'zip' => 'required',
            'country' => 'required|not_in:0'
        );


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
        }
        else{

            $mailchimp_requests = EblixMailchimp::mailchimp_curl_requests($request, 'POST', 'lists');
            $mailchimp_responce = json_decode($mailchimp_requests);

            if (is_object($mailchimp_responce) && array_key_exists('id', $mailchimp_responce)){
                $List = new MailchimpList();
                $List->list_id = $mailchimp_responce->id;
                $List->name = $request->list_name;
                $List->from_email = $request->from_email;
                $List->from_name = $request->from_name;
                $List->subject = $request->subject;
                $List->language = $request->language;
                $List->email_type_option = ($request->email_type_option) ? true : false;
                $List->permission_reminder = $request->permission_reminder;
                $List->company = $request->company;
                $List->address1 = $request->address1;
                $List->address2 = $request->address2;
                $List->city = $request->city;
                $List->state = $request->state;
                $List->zip = $request->zip;
                $List->country = $request->country;
                $List->phone = $request->phone;
                $List->save();

                $request->session()->flash('message', "List Created Successfully!");
                return Redirect::to('/mailchimp');
            }else{

                $request->session()->flash('message', "List Created Unsuccessfully!");
                return Redirect::to('/mailchimp');
            }
        }
    }

    public function edit($id)
    {
        $list = MailchimpList::find($id);
        return view('eblix.mailchimp.lists.edit', ['list' => $list, 'countries' => Country::all()]);
    }

    public function update(Request $request, $id)
    {
        $List = MailchimpList::find($id);

        $rules = array(
            'list_name' => 'required',
            'from_email' => 'required|email',
            'from_name' => 'required',
            'subject' => 'required',
            'company' => 'required',
            'address1' => 'required',
            'address2' => 'required',
            'permission_reminder' => 'required',
            'city' => 'required',
            'state' => 'required',
            'language' => 'required|not_in:0',
            'zip' => 'required',
            'country' => 'required|not_in:0'
        );


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
        }
        else{

            $mailchimp_requests = EblixMailchimp::mailchimp_curl_requests($request, 'PATCH', 'lists', $List->list_id);
            $mailchimp_responce = json_decode($mailchimp_requests);

            if (is_object($mailchimp_responce) && array_key_exists('id', $mailchimp_responce)){
                $List->list_id = $mailchimp_responce->id;
                $List->name = $request->list_name;
                $List->from_email = $request->from_email;
                $List->from_name = $request->from_name;
                $List->subject = $request->subject;
                $List->language = $request->language;
                $List->email_type_option = ($request->email_type_option) ? true : false;
                $List->permission_reminder = $request->permission_reminder;
                $List->company = $request->company;
                $List->address1 = $request->address1;
                $List->address2 = $request->address2;
                $List->city = $request->city;
                $List->state = $request->state;
                $List->zip = $request->zip;
                $List->country = $request->country;
                $List->phone = $request->phone;
                $List->update();

                $request->session()->flash('message', "List Updated Successfully!");

                return Redirect::to('/mailchimp');
            }else{

                $request->session()->flash('message', "List Updated Unsuccessfully!");
                return Redirect::to('/mailchimp');
            }
        }
    }

    public function destroy(Request $request, $id){
        $List = MailchimpList::find($id);

        $mailchimp_requests = EblixMailchimp::mailchimp_curl_requests($request, 'DELETE', 'lists', $List->list_id);
        $mailchimp_responce = json_decode($mailchimp_requests);

        if($mailchimp_responce == 204){
            $List->members()->delete();
            $List->delete();
            $request->session()->flash('message', "List Deleted Successfully!");
        }
        else{
            $request->session()->flash('message', "List Deleted Unsuccessfully!");
        }

        return Redirect::to('/mailchimp');
    }
}