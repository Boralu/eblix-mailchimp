<?php
namespace Eblix\Mailchimp\Controllers;

use Eblix\Mailchimp\Models\MailchimpList;
use Eblix\Mailchimp\Models\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Eblix\Mailchimp\EblixMailchimp;

class EblixMembersController
{
    public function index($list){
        $list = MailchimpList::find($list);
        return view('eblix.mailchimp.members.list', ['members' => Member::where('list_id', $list->id)->get(), 'list' => $list]);
    }

    public function create($list)
    {
        $list = MailchimpList::find($list);
        return view('eblix.mailchimp.members.create', ['list' => $list]);
    }

    public function store(Request $request, $list){
        $list = MailchimpList::find($list);

        $rules = array(
            'first_name' => 'required',
            'email_address' => 'required|email',
            'last_name' => 'required'
        );


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
        }
        else{

            $mailchimp_requests = EblixMailchimp::mailchimp_curl_requests($request, 'POST', 'members', $list->list_id);
            $mailchimp_responce = json_decode($mailchimp_requests);

            if (is_object($mailchimp_responce) && array_key_exists('id', $mailchimp_responce)){
                $Member = new Member();
                $Member->member_id = $mailchimp_responce->id;
                $Member->first_name = $request->first_name;
                $Member->last_name = $request->last_name;
                $Member->email_address = $request->email_address;
                $Member->list_id = $list->id;
                $Member->save();

                $request->session()->flash('message', "Member Created Successfully!");
                return Redirect::to('/mailchimp/'.$list->id.'/members/');
            }else{

                $request->session()->flash('message', "Member Created Unsuccessfully!");
                return Redirect::to('/mailchimp/'.$list->id.'/members/');
            }
        }
    }

    public function edit($list, $member)
    {
        $list = MailchimpList::find($list);
        $member = Member::find($member);
        return view('eblix.mailchimp.members.edit', ['list' => $list, 'member' => $member]);
    }

    public function update(Request $request, $list, $member)
    {
        $list = MailchimpList::find($list);
        $Member = Member::find($member);

        $rules = array(
            'first_name' => 'required',
            'email_address' => 'required|email',
            'last_name' => 'required'
        );


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
        }
        else{

            $mailchimp_requests = EblixMailchimp::mailchimp_curl_requests($request, 'PATCH', 'members', $list->list_id, $Member->member_id);
            $mailchimp_responce = json_decode($mailchimp_requests);

            if (is_object($mailchimp_responce) && array_key_exists('id', $mailchimp_responce)){
                $Member->member_id = $mailchimp_responce->id;
                $Member->first_name = $request->first_name;
                $Member->last_name = $request->last_name;
                $Member->email_address = $request->email_address;
                $Member->list_id = $list->id;
                $Member->update();

                $request->session()->flash('message', "Member Updated Successfully!");
                return Redirect::to('/mailchimp/'.$list->id.'/members/');
            }else{

                $request->session()->flash('message', "Member Updated Unsuccessfully!");
                return Redirect::to('/mailchimp/'.$list->id.'/members/');
            }
        }
    }

    public function destroy(Request $request, $list, $member){
        $list = MailchimpList::find($list);
        $Member = Member::find($member);

        $mailchimp_requests = EblixMailchimp::mailchimp_curl_requests($request, 'DELETE', 'members', $list->list_id, $Member->member_id);
        $mailchimp_responce = json_decode($mailchimp_requests);

        if($mailchimp_responce == 204){
            $request->session()->flash('message', "Member Deleted Successfully!");
            $Member->delete();
        }
        else{
            $request->session()->flash('message', "Member Deleted Unsuccessfully!");
        }

        return Redirect::to('/mailchimp/'.$list->id.'/members/');
    }
}