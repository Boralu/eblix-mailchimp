<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailchimpMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailchimp_members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_id', 40);
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->string('email_address', 255);
            $table->integer('list_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailchimp_members');
    }
}
