<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    public function up()
    {
        Schema::create('countries', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('short_name',2);
            $table->string('long_name', 255);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('countries');
    }
}