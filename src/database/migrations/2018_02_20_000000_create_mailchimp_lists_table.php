<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailchimpListsTable extends Migration
{
    public function up()
    {
        Schema::create('mailchimp_lists', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('list_id',10);
            $table->string('name', 255);
            $table->string('from_email', 255);
            $table->string('from_name', 255);
            $table->text('subject');
            $table->string('language',2)->default('en');
            $table->boolean('email_type_option')->default(false);
            $table->text('permission_reminder')->nullable();
            $table->string('company',255);
            $table->string('address1',255);
            $table->string('address2',255);
            $table->string('city',255);
            $table->string('state',255);
            $table->string('zip',255);
            $table->string('country', 2);
            $table->string('phone', 15);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('mailchimp_lists');
    }
}