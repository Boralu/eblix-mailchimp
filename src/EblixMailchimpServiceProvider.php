<?php
namespace Eblix\Mailchimp;

use Illuminate\Support\ServiceProvider;

class EblixMailchimpServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'mailchimp');

        $this->publishes([
            __DIR__ . '/config/mailchimp.php' => config_path('mailchimp.php'),
            __DIR__.'/assets/css' => public_path('eblix/mailchimp'),
            __DIR__.'/views' => base_path('resources/views/eblix/mailchimp'),
        ]);

        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->config->get('mailchimp') === null) {
            $this->app->config->set('mailchimp', require __DIR__ . '/config/mailchimp.php');
        }

        $this->app->singleton(EblixMailchimp::class, function () {
            return new EblixMailchimp();
        });

        $this->app->alias(EblixMailchimp::class, 'eblix-mailchimp');
    }
}