<?php

Route::group(['middleware' => 'web'], function () {
    Route::get('/mailchimp', '\Eblix\Mailchimp\Controllers\EblixMailchimpController@index');
    Route::get('/mailchimp/create', '\Eblix\Mailchimp\Controllers\EblixMailchimpController@create');
    Route::post('/mailchimp/create', '\Eblix\Mailchimp\Controllers\EblixMailchimpController@store');

    Route::get('/mailchimp/{id}/edit', '\Eblix\Mailchimp\Controllers\EblixMailchimpController@edit');
    Route::post('/mailchimp/{id}/update', '\Eblix\Mailchimp\Controllers\EblixMailchimpController@update');
    Route::get('/mailchimp/{id}/delete', '\Eblix\Mailchimp\Controllers\EblixMailchimpController@destroy');

    Route::get('/mailchimp/{list}/members', '\Eblix\Mailchimp\Controllers\EblixMembersController@index');
    Route::get('/mailchimp/{list}/members/create', '\Eblix\Mailchimp\Controllers\EblixMembersController@create');
    Route::post('/mailchimp/{list}/members/create', '\Eblix\Mailchimp\Controllers\EblixMembersController@store');

    Route::get('/mailchimp/{list}/members/{member}/edit', '\Eblix\Mailchimp\Controllers\EblixMembersController@edit');
    Route::post('/mailchimp/{list}/members/{member}/update', '\Eblix\Mailchimp\Controllers\EblixMembersController@update');
    Route::get('/mailchimp/{list}/members/{member}/delete', '\Eblix\Mailchimp\Controllers\EblixMembersController@destroy');
});