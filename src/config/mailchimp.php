<?php
return [

    'MAILCHIMP_API_KEY' => 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX-usXX', //Your Mailchimp API Key which is required

    //Your Contact & Other Details

    'CONTACT_COMPANY' => 'Company Name',
    'CONTACT_ADDRESS1' => 'Address',
    'CONTACT_ADDRESS2' => 'Address',
    'CONTACT_CITY' => 'City',
    'CONTACT_STATE' => 'State',
    'CONTACT_ZIP' => 'Zip or postal',
    'CONTACT_COUNTRY' => 'US',
    'PERMISSION_REMINDER' => 'You are receiving this email because you opted in via our website.',
    'EMAIL_TYPE_OPTION' => true,
    'CAMPAIGN_FROM_NAME' => "Form Name",
    'CAMPAIGN_FROM_EMAIL' => "emailaddress@email.com",
    'CAMPAIGN_SUBJECT' => "",
    'CAMPAIGN_LANGUAGE' => "en",
];