<?php
namespace Eblix\Mailchimp;

use Illuminate\Support\Facades\Facade;

class EblixMailchimpFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'eblix-mailchimp';
    }
}