<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="{{ asset('eblix/mailchimp/bootstrap.min.css')}}">
</head>
<body>

<div class="container-fluid">
    <div class="page-header">
        <h2>Create List</h2>
        <a href="{{url('/mailchimp')}}" class="btn btn-success">View Lists</a>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <h3>List Details</h3>
        </div>
    </div>

    <form action="{{url('/mailchimp/create')}}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('list_name') ? ' has-error' : '' }}">
                    <label for="list_name">List Name</label>
                    <input type="text" class="form-control" id="list_name" name="list_name" value="{{old('list_name')}}">
                    @if ($errors->has('list_name'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('list_name') }}</label>
                    @endif
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group {{ $errors->has('from_email') ? ' has-error' : '' }}">
                    <label for="from_email">Default From email address</label>
                    <input type="email" class="form-control" id="from_email" name="from_email" value="{{old('from_email')}}">
                    @if ($errors->has('from_email'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('from_email') }}</label>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('from_name') ? ' has-error' : '' }}">
                    <label for="from_name">Default From Name</label>
                    <input type="text" class="form-control" id="from_name" name="from_name" value="{{old('from_name')}}">
                    @if ($errors->has('from_name'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('from_name') }}</label>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                    <label for="subject">Subject</label>
                    <input type="text" class="form-control" id="subject" name="subject" value="{{old('subject')}}">
                    @if ($errors->has('subject'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('subject') }}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('language') ? ' has-error' : '' }}">
                    <label for="language">Language</label>
                    <select class="form-control" id="language" name="language" data-placeholder="Choose a Language...">
                        <option value="EN">English</option>
                    </select>
                    @if ($errors->has('language'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('language') }}</label>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('email_type_option') ? ' has-error' : '' }}">
                    <label>Email Type Option</label>
                    <div class="checkbox">
                        <label><input type="checkbox" name="email_type_option" {{ (config('mailchimp.EMAIL_TYPE_OPTION')) ? 'checked' : '' }}>Enable</label>
                        @if ($errors->has('email_type_option'))
                            <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('email_type_option') }}</label>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('permission_reminder') ? ' has-error' : '' }}">
                    <label>Permission Reminder</label>
                    <textarea class="form-control" name="permission_reminder"></textarea>
                    @if ($errors->has('permission_reminder'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('permission_reminder') }}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <h3>Contact Details</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('company') ? ' has-error' : '' }}">
                    <label for="company">Company</label>
                    <input class="form-control" type="text" id="company" name="company" value="{{config('mailchimp.CONTACT_COMPANY')}}" >
                    @if ($errors->has('company'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('company') }}</label>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('address1') ? ' has-error' : '' }}">
                    <label for="address1">Address 1</label>
                    <input class="form-control" type="text" id="address1" name="address1" value="{{config('mailchimp.CONTACT_ADDRESS1')}}" >
                    @if ($errors->has('address1'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('address1') }}</label>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('address2') ? ' has-error' : '' }}">
                    <label for="address2">Address 2</label>
                    <input class="form-control" type="text" id="address2" name="address2" value="{{config('mailchimp.CONTACT_ADDRESS2')}}">
                    @if ($errors->has('address2'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('address2') }}</label>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                    <label for="city">City</label>
                    <input class="form-control" type="text" id="city" name="city" value="{{config('mailchimp.CONTACT_CITY')}}">
                    @if ($errors->has('city'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('city') }}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('state') ? ' has-error' : '' }}">
                    <label for="state">State</label>
                    <input class="form-control" type="text" id="state" name="state" value="{{config('mailchimp.CONTACT_STATE')}}">
                    @if ($errors->has('state'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('state') }}</label>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('zip') ? ' has-error' : '' }}">
                    <label for="zip">Zip Code</label>
                    <input class="form-control" type="number" id="zip" name="zip" value="{{config('mailchimp.CONTACT_ZIP')}}">
                    @if ($errors->has('zip'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('zip') }}</label>
                    @endif
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                    <label for="country">Country</label>
                    <select class="form-control" name="country" id="country">
                        <option value="0">select Country</option>
                        @if(count($countries) > 0)
                            @foreach($countries as $country)
                                <option value="{{$country->short_name}}" {{ (old('country') == $country->short_name) ? 'selected' : '' }}>{{$country->long_name}}</option>
                            @endforeach
                        @endif
                    </select>
                    @if ($errors->has('country'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('country') }}</label>
                    @endif
                </div>
            </div>
            <div class="col-md-3 {{ $errors->has('phone') ? ' has-error' : '' }}">
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input class="form-control" type="number" id="phone" name="phone">
                    @if ($errors->has('phone'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('phone') }}</label>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1 pull-right">
                <button class="btn btn-success pull-right">Save</button>
            </div>
        </div>
    </form>
</div>

</body>
</html>
