<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="{{ asset('eblix/mailchimp/bootstrap.min.css')}}">

</head>
<body>
<div class="container-fluid">
    <div class="page-header">
        <h2>List</h2>
        <a href="{{url('/mailchimp/create')}}" class="btn btn-success">New List</a>
    </div>
</div>
<div class="col-lg-12">

    @if (Session::has('message') && Session::get('message'))
        {{Session::get('message')}}
    @endif

    <table class="table table-bordered">
        <thead>
        <tr>
            <th width="10%">List ID</th>
            <th>List Name</th>
            <th>From Name</th>
            <th>From Email</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @if(count($lists) > 0)
            @foreach($lists as $list)
                <tr>
                    <td>{{$list->list_id}}</td>
                    <td>{{$list->name}}</td>
                    <td>{{$list->from_name}}</td>
                    <td>{{$list->from_email}}</td>
                    <td width="10%">
                        <a href="{{url('/mailchimp/'.$list->id.'/edit')}}" class="btn">Update</a>
                        <a href="{{url('/mailchimp/'.$list->id.'/delete')}}" class="btn">Delete</a>
                        <a href="{{url('/mailchimp/'.$list->id.'/members')}}" class="btn">Members</a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">
                    No Lists Found!
                </td>
            </tr>
        @endif

        </tbody>
    </table>
</div>
</body>
</html>
