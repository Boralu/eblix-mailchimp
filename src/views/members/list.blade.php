<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="{{ asset('eblix/mailchimp/bootstrap.min.css')}}">

</head>
<body>
<div class="container-fluid">
    <div class="page-header">
        <h2>Members <small>({{$list->name}})</small></h2>
        <a href="{{url('/mailchimp')}}" class="btn btn-success">View Lists</a>
        <a href="{{url('/mailchimp/'.$list->id.'/members/create')}}" class="btn btn-success">New Member</a>
    </div>
</div>
<div class="col-lg-12">

    @if (Session::has('message') && Session::get('message'))
        {{Session::get('message')}}
    @endif

    <table class="table table-bordered">
        <thead>
        <tr>
            <th width="10%">Member ID</th>
            <th>Email Address</th>
            <th>Name</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @if(count($members) > 0)
            @foreach($members as $member)
                <tr>
                    <td>{{$member->member_id}}</td>
                    <td>{{$member->email_address}}</td>
                    <td>{{$member->first_name}} {{$list->last_name}}</td>
                    <td width="10%">
                        <a href="{{url('/mailchimp/'.$list->id.'/members/'.$member->id.'/edit')}}" class="btn">Update</a>
                        <a href="{{url('/mailchimp/'.$list->id.'/members/'.$member->id.'/delete')}}" class="btn">Delete</a>
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4">
                    No Members Found!
                </td>
            </tr>
        @endif

        </tbody>
    </table>
</div>
</body>
</html>
