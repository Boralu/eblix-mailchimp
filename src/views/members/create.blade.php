<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="{{ asset('eblix/mailchimp/bootstrap.min.css')}}">
</head>
<body>

<div class="container-fluid">
    <div class="page-header">
        <h2>Create Member <small>({{$list->name}})</small></h2>
        <a href="{{url('/mailchimp')}}" class="btn btn-success">View Lists</a>
        <a href="{{url('/mailchimp/'.$list->id.'/members')}}" class="btn btn-success">View Members</a>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <h3>Member Details</h3>
        </div>
    </div>

    <form action="{{url('/mailchimp/'.$list->id.'/members/create')}}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                    <label for="list_name">First Name</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" value="{{old('first_name')}}">
                    @if ($errors->has('first_name'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('first_name') }}</label>
                    @endif
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label for="list_name">Last Name</label>
                    <input type="text" class="form-control" id="last_name" name="last_name" value="{{old('last_name')}}">
                    @if ($errors->has('last_name'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('last_name') }}</label>
                    @endif
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group {{ $errors->has('email_address') ? ' has-error' : '' }}">
                    <label for="from_email">Email Address</label>
                    <input type="email" class="form-control" id="email_address" name="email_address" value="{{old('email_address')}}">
                    @if ($errors->has('email_address'))
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $errors->first('email_address') }}</label>
                    @endif
                </div>
            </div>


        </div>

        <div class="row">
            <div class="col-md-1 pull-right">
                <button class="btn btn-success pull-right">Save</button>
            </div>
        </div>
    </form>
</div>

</body>
</html>
