<?php
namespace Eblix\Mailchimp;

class EblixMailchimp
{
    public static function mailchimp_curl_requests($request, $request_type, $method, $list_id = null, $member_id = null){
        $api_key = config('mailchimp.MAILCHIMP_API_KEY');
        $dc = substr($api_key,strpos($api_key,'-')+1);
        $url = 'https://'.$dc.'.api.mailchimp.com/3.0/lists/';

        if($list_id != null){
            $url .= $list_id;
        }

        if($method == 'members'){
            $url .= '/members';
            $data = self::get_mailchimp_member_json_object($request);

            if($member_id != null){
                $url .= '/'.$member_id;
            }
        }
        else{
            $data = self::get_mailchimp_list_json_object($request);
        }

        $mch = curl_init();
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic '.base64_encode( 'user:'. $api_key )
        );


        curl_setopt($mch, CURLOPT_URL, $url );
        curl_setopt($mch, CURLOPT_HTTPHEADER, $headers);
        //curl_setopt($mch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
        curl_setopt($mch, CURLOPT_RETURNTRANSFER, true); // do not echo the result, write it into variable
        curl_setopt($mch, CURLOPT_CUSTOMREQUEST, $request_type); // according to MailChimp API: POST/GET/PATCH/PUT/DELETE
        curl_setopt($mch, CURLOPT_TIMEOUT, 10);
        curl_setopt($mch, CURLOPT_SSL_VERIFYPEER, false); // certificate verification for TLS/SSL connection

        switch ($request_type){
            case 'GET':
                break;

            case 'DELETE':
                $result = curl_exec($mch);
                $result = curl_getinfo($mch, CURLINFO_HTTP_CODE);
                break;

            default :
                curl_setopt($mch, CURLOPT_POST, true);
                curl_setopt($mch, CURLOPT_POSTFIELDS, $data ); // send data in json

                $result = curl_exec($mch);
                break;
        }

        return $result;
    }

    private static function get_mailchimp_list_json_object($request){
        $data = [];
        $data["name"] = $request->list_name;
        $data["contact"] = array(
            "company" => $request->company,
            "address1"=> $request->address1,
            "address2"=> $request->address2,
            "city"=> $request->city,
            "state"=> $request->state,
            "zip"=> $request->zip,
            "country"=> $request->country,
            "phone"=> $request->phone
        );
        $data["permission_reminder"] = $request->permission_reminder;
        $data["email_type_option"] = ($request->email_type_option) ? true : false;
        $data["campaign_defaults"] = array(
            "from_name"=> $request->from_name,
            "from_email"=> $request->from_email,
            "subject"=> $request->subject,
            "language"=> $request->language
        );

        return json_encode($data);
    }

    private static function get_mailchimp_member_json_object($request){
        $data = [];
        $data["email_address"] = $request->email_address;
        $data["status"] = "subscribed";
        $data["merge_fields"] = array(
            "FNAME"=> $request->first_name,
            "LNAME"=> $request->last_name
        );

        return json_encode($data);
    }
}