<?php

namespace Eblix\Mailchimp\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'mailchimp_members';
}
