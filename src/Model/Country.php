<?php

namespace Eblix\Mailchimp\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
}
