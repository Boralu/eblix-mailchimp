<?php
namespace Eblix\Mailchimp\Models;

use Illuminate\Database\Eloquent\Model;

class MailchimpList extends Model
{
    protected $table = 'mailchimp_lists';

    public function members(){
        return $this->hasMany('Eblix\Mailchimp\Models\Member', 'list_id');
    }
}
